﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RESTDemo2.Models;

namespace RESTDemo2.DataAccessLayer
{
	public class StorageDBInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<StorageDB>
	{
		protected override void Seed(StorageDB context)
		{
			var products = new List<Product>
			{
				new Product{ProductId = Guid.NewGuid(), ProductName = "Adjustable Race", ProductNumber = "AR-5381", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Bearing Ball", ProductNumber = "BA-8327", Stock = 500, DaysToManufacture = 2, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "BB Ball Bearing", ProductNumber = "BE-2349", Stock = 200, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Headset Ball Bearings", ProductNumber = "BE-2908", Stock = 300, DaysToManufacture = 3, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Blade", ProductNumber = "BL-2036", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "LL Crankarm", ProductNumber = "CA-5965", Stock = 100, DaysToManufacture = 4, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "ML Crankarm", ProductNumber = "CA-6738", Stock = 280, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "HL Crankarm", ProductNumber = "CA-7457", Stock = 420, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Chainring Bolts", ProductNumber = "CB-2903", Stock = 540, DaysToManufacture = 2, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Chainring Nut", ProductNumber = "CN-6137", Stock = 550, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Chainring", ProductNumber = "CR-7833", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Crown Race", ProductNumber = "CR-9981", Stock = 260, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Chain Stays", ProductNumber = "CS-2812", Stock = 300, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Mountain End Caps", ProductNumber = "EC-M092", Stock = 600, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Road End Caps", ProductNumber = "EC-R098", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Front Derailleur Cage", ProductNumber = "FC-3982", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Guide Pulley", ProductNumber = "GP-0982", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Keyed Washer", ProductNumber = "KW-4091", Stock = 500, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Lower Head Race", ProductNumber = "LR-8520", Stock = 200, DaysToManufacture = 1, ActiveStatus = true},
				new Product{ProductId = Guid.NewGuid(), ProductName = "Reflector", ProductNumber = "RF-9198", Stock = 450, DaysToManufacture = 1, ActiveStatus = true}
			};
			products.ForEach(p => context.Product.Add(p));
			context.SaveChanges();
		}
	}
}