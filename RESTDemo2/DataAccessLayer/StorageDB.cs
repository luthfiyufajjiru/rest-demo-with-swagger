﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using RESTDemo2.Models;

namespace RESTDemo2.DataAccessLayer
{
	public class StorageDB : DbContext
	{
		public StorageDB() : base("FormulatrixDemoAPI")
		{
		}

		public DbSet<Product> Product { get; set; }
	}
}